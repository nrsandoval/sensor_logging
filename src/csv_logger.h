/**
 * @file csv_logger.h
 *
 * @brief Functions that are used to log to the console in a csv format
 *
 * The csv format is 2 columns and consists of a time column and data column
 *
 * @note This functionality is tailored for an Arduino system
 *
 * @author N_S
 * @year 2019
 * @version 1.0
 */

/** 
 * @brief toggles hardware or software serial output
 * @note software serial uses pin 9 and 10
 */
#define LOGGER_USE_HARDWARE_SERIAL

/**
 * @brief Logs data by returning it to the serial console
 *
 */

/*** end of file ***/
