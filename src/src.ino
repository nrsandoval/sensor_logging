/**
 * @file src.ino
 *
 * src file for the sensor logger
 *
 * @author Nicholas Sandoval
 * @date January 11, 2019
 */

#include <Arduino.h>

// Globals
#define PRESSURE_SENSOR
#ifdef PRESSURE_SENSOR
#include <Adafruit_MPRLS.h>
// Reset and EOC pin unnecessary
#define RESET_PIN -1 // Set to hard reset at begin
#define EOC_PIN -1   // Set to read end-of-conversation by pin
Adafruit_MPRLS mpr = Adafruit_MPRLS(RESET_PIN, EOC_PIN);
#endif /* PRESSURE_SENSOR */

bool time;

float offset = 0;

void setup()
{
   Serial.begin(9600);
   Serial.println("Sensor logger");

   if (! mpr.begin())
   {
      Serial.println("Failed to communicate with MPRLS sensor!!");
      while(1);
   }
   Serial.println("Found MPRLS sensor");

   // Calibrate
   float pressure_psi = mpr.readPressure()/68.947572932;

   int i;
   float samples[5];
   for (i = 0; i < 5; i++)
   {
      samples[i] = mpr.readPressure()/68.947572932;
   }

   for (i = 0; i < 5; i++)
      offset += samples[i];

   offset = (offset / 5) - 0.001;
   Serial.print("Offset: "); Serial.print(offset);
}

void loop()
{
   float pressure_psi = mpr.readPressure()/68.947572932;

   Serial.print(millis()); Serial.print(",");
   
   // For PSI
   Serial.println(pressure_psi - offset);

   if (Serial.available())
   {
      Serial.println("Logging complete");
      while(1);
   }
}

/*** end of file ***/

