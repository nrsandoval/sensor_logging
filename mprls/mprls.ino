/*!
 * @file mprls_simpletest.ino
 *
 * A basic test of the sensor with default settings
 * 
 * Designed specifically to work with the MPRLS sensor from Adafruit
 * ----> https://www.adafruit.com/products/3965
 *
 * These sensors use I2C to communicate, 2 pins (SCL+SDA) are required
 * to interface with the breakout.
 *
 * Adafruit invests time and resources providing this open source code,
 * please support Adafruit and open-source hardware by purchasing
 * products from Adafruit!
 *
 * Written by Limor Fried/Ladyada for Adafruit Industries.  
 *
 * MIT license, all text here must be included in any redistribution.
 *
 */
 
#include <Wire.h>
#include "Adafruit_MPRLS.h"

// You dont *need* a reset and EOC pin for most uses, so we set to -1 and don't connect
#define RESET_PIN  -1  // set to any GPIO pin # to hard-reset on begin()
#define EOC_PIN    -1  // set to any GPIO pin to read end-of-conversion by pin
Adafruit_MPRLS mpr = Adafruit_MPRLS(RESET_PIN, EOC_PIN);

float offset = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("MPRLS Simple Test");
  if (! mpr.begin()) {
    Serial.println("Failed to communicate with MPRLS sensor, check wiring?");
  }
  Serial.println("Found MPRLS sensor");

  // Calculate offset
  float samples[5];
  for (int i = 0; i < 5; i++)
  {
    samples[i] = mpr.readPressure()/68.947572932;
  }

  for (int i = 0; i < 5; i++)
  {
    offset += samples[i];
  }

  // -0.001 is used to not have the values dip below zero
  offset = (offset / 5) - 0.001;
  
  //Serial.print("Offset: "); Serial.println(offset); // Used for debugging
}


void loop() {
  float pressure_psi = mpr.readPressure()/68.947572932;
  Serial.print(millis()); Serial.println(pressure_psi - offset);
}
