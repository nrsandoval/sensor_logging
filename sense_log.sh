#!/bin/sh

picocom -b 9600 /dev/ttyACM0 | tee output.csv

#cp output.csv output_copy.csv

vim output.csv <<end
/Found
dgg
/Logging
dG
:wq
end

libreoffice --calc --norestore output.csv &
