##Required libraries:

#FreqMeasure
- Uses pin 8 for frequency input on arduino UNO
- Used white plastic sensor

#MPRLS library
- Pressure sensor

#HX711_ADC
- HX711 library for weight sensors

##Applications:

#sdp
- Serial plotter that is able to save data
- included in apps/ folder
- Make sure to edit the .ini file for your particular computer
   - Mainly for selecting which port the arduino is connected to
